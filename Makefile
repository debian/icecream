
PREFIX=/usr/local

BIN=icecream
MAN=icecream.1

all:

install:
	install -m 0755 $(BIN) $(PREFIX)/bin
	install -m 0644 $(MAN) $(PREFIX)/man/man1

icecream (1.3-6) unstable; urgency=medium

  * QA upload.

  * Updated d/gbp.conf to enforce the use of pristine-tar.
  * Updated Standards-Version from 3.9.2 to 4.7.0.
  * Use wrap-and-sort -at for debian control files
  * Use versioned copyright format URI.
  * Update copyright file header to use current field names
    (Name ⇒ Upstream-Name, Maintainer ⇒ Upstream-Contact).
  * Remove malformed and unnecessary DM-Upload-Allowed field
    in debian/control.
  * Bump debhelper from deprecated 7 to 13.
  * Use canonical URL in Vcs-Browser and switched Vcs links to Salsa.
  * Flagged icecream binary package as Multi-Arch: foreign.
  * Flagged in d/control that no root build is needed.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 19 May 2024 07:57:46 +0200

icecream (1.3-5) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group <packages@qa.debian.org>. (see: #1003230)

 -- Marcos Talau <talau@debian.org>  Tue, 15 Nov 2022 10:44:33 -0300

icecream (1.3-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 11:58:17 +0100

icecream (1.3-4) unstable; urgency=low

  * debian/control: bump up Standards-Version to 3.9.2.
  * fix_time_limit_handling.patch: fix the patch, which
    was incorrectly commenting a line (Closes: #627893).

 -- Cristian Greco <cristian@regolo.cc>  Fri, 27 May 2011 19:37:44 +0200

icecream (1.3-3) unstable; urgency=low

  * Convert package to 3.0 (quilt) source format.
  * debian/control:
    - new maintainer email address.
    - bump standards-version to 3.8.3 (no changes).
    - setting DMUA, thanks to Xavier Oswald.
  * debian/patches:
    - fix_tracks_typo.patch: new patch, fix a typo (possible bug).
    - fix_time_limit_handling.patch: new patch, fix time limit options
      handling. (LP: #484826)

 -- Cristian Greco <cristian@regolo.cc>  Tue, 22 Dec 2009 12:59:17 +0100

icecream (1.3-2) unstable; urgency=low

  * debian/control:
    - new maintainer email address;
    - add Vcs-* stuff (git collab-maint maintenance);
    - bump standards-version to 3.8.2 (no changes required);
    - add ${misc:Depends}.
  * debian/patches:
    - switch from dpatch to quilt.
    - added fix-makefile.patch and deleted the old one.
  * debian/{control,rules}: build-depends on debhelper (>= 7.0.50) and
    quilt (>= 0.46-7) and rewrote debian/rules in a shortest form.
  * debian/compat: switch to 7.
  * debian/manpages: deleted.
  * debian/dirs: deleted.
  * debian/copyright: update to new format specifications.

 -- Cristian Greco <cristian.debian@gmail.com>  Thu, 23 Jul 2009 19:47:34 +0200

icecream (1.3-1) unstable; urgency=low

  * New Maintainer (Closes: #431684)
  * New upstream release (including a patch of mine)
    - Parse .m3u playlists starting with a comment (Closes: #396892)
    - Fix wrong requests to server (Closes: #396893)
    - Support direct stream URLs (Closes: #319342)
    - New features and bugfixes.
    - Fix manpage mistake (now use section 1 instead of 8)
  * debian/control:
    - Bump Standards-Version to 3.7.3.
    - Moved the "Homepage:" pseudo-header from the extended description
      to the new field in the source package general paragraph
    - Modified short and extended Description
  * debian/copyright:
    - Modified homepage
    - Updated years for the copyright
    - Included GPL headers
    - Included a license statement for Debian packaging
  * debian/dirs: deleted 'usr/share/man/man8'
  * debian/manpages: list new manpage
  * debian/patches:
    - remove 10-manpage-fix.dpatch, fixed upstream
    - add description to 20-makefile-debianlike.dpatch
  * debian/rules: binary-arch must exists, even if it is empty (policy 4.9)
  * debian/watch: added

 -- Cristian Greco <cgreco@cs.unibo.it>  Tue, 29 Apr 2008 09:43:16 +0200

icecream (1.2-3) unstable; urgency=low

  * QA upload.
  * Launch dh_md5sums in debian/rules.

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Sun, 19 Aug 2007 17:53:30 +0200

icecream (1.2-2) unstable; urgency=low

  * QA upload.
  * Package is orphaned (#431684); set maintainer to Debian QA Group.
  * debian/rules: Remove no-op invocation of `-$(MAKE) clean'.

 -- Matej Vela <vela@debian.org>  Thu, 19 Jul 2007 10:15:32 +0200

icecream (1.2-1) unstable; urgency=low

  * New upstream release
  * Adopt package (Closes: #375009)
  * Changed Debhelper compatibility level to 5
  * Updated authors email
  * Changed Build-Depends-Indep to Build-Depends

 -- Marvin Stark <marv@der-marv.de>  Mon, 26 Jun 2006 21:17:57 +0200

icecream (0.8-3) unstable; urgency=low

  * -s is no longer mentioned (matched --stop and --stdout, Closes: #230508)
  * Sponsored by Gerfried Fuchs <alfie@debian.org>, like previous upload.

 -- Christoph Siess (CHS) <chs@geekhost.info>  Sat, 31 Jan 2004 14:20:33 +0100

icecream (0.8-2) unstable; urgency=low

  * icecream is binary all, not any (Closes: #225222.)
  * I forgot to close the ITP (Closes: #220937.)
  * install uses upstreams install target now
  * changed Build-Depends to Build-Depends-Indep

 -- Christoph Siess (CHS) <chs@geekhost.info>  Sat, 27 Dec 2003 17:49:35 +0100

icecream (0.8-1) unstable; urgency=low

  * Initial Release.

 -- Christoph Siess (CHS) <chs@geekhost.info>  Sat, 27 Dec 2003 16:31:38 +0100
